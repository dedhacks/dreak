package com.jcardonne.dreak;

import com.jcardonne.dreak.commands.InteractiveChatCommand;
import com.jcardonne.dreak.commands.NotificationCommand;
import com.jcardonne.dreak.commands.PingCommand;
import com.jcardonne.dreak.commands.RandomTeleportCommand;
import com.jcardonne.dreak.event.*;
import org.mineacademy.fo.plugin.SimplePlugin;

public class DreakPlugin extends SimplePlugin {

	@Override
	protected void onPluginStart() {

		//COMMANDS HERE <----------------------------
		registerCommand(new RandomTeleportCommand());
		registerCommand(new InteractiveChatCommand());
		registerCommand(new NotificationCommand());
		registerCommand(new PingCommand());


		//LISTENER HERE <----------------------------
		registerEvents(new RespawnListener());
		registerEvents(new ArrowBreakingWindowsListener());
		registerEvents(new FastFoodListener());
		registerEvents(new CustomHealthListener());
		registerEvents(new TreeListener());
	}

}
