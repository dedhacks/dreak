package com.jcardonne.dreak.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.mineacademy.fo.remain.Remain;

public final class RespawnListener implements Listener {

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player player = event.getEntity(); //getEntity() due to an extension to EntityDeathEvent (PlayerDeathEvent)
		event.setDeathMessage(null);
		Remain.respawn(player, 2);
	}

	/* vvvvvvvvvv---- need a location in case of death
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		final Location location = new Location(event.getPlayer().getWorld(), 1, 2, 3);
		event.setRespawnLocation(location);
	} */
}
