package com.jcardonne.dreak.event;

import lombok.Data;
import net.sacredlabyrinth.phaed.simpleclans.events.PlayerJoinedClanEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.collection.StrictMap;
import org.mineacademy.fo.remain.CompAttribute;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class CustomHealthListener implements Listener {

	private final StrictMap<String, Integer> worldHealthMap = new StrictMap<>();
	private final StrictMap<UUID, Set<WorldHealth>> playerHealthMap = new StrictMap<>();

	public CustomHealthListener() {
		worldHealthMap.put("world", 40);
		worldHealthMap.put("nature", 10); //<- for severals worlds. Format: ("worldName", halfHeart)
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		adjustHealth(event.getPlayer());
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event) {
		//Store health btw world
		Player player = event.getPlayer();
		Set<WorldHealth> worldHealthSet = playerHealthMap.getOrPut(player.getUniqueId(),new HashSet<>());

		worldHealthSet.add(new WorldHealth(event.getFrom().getName(),player.getHealth()));
		adjustHealth(event.getPlayer());

		for (WorldHealth worldHealth : worldHealthSet)
			if(worldHealth.getName().equals(player.getWorld().getName())) {
				worldHealthSet.remove(worldHealth);

				Common.log("Setting new health to " + worldHealth.getHealth());

				player.setHealth(worldHealth.getHealth());
				break;
			}
	}

	private void adjustHealth(Player player) {
		Integer health = worldHealthMap.get(player.getWorld().getName());

		if (health != null)
			CompAttribute.GENERIC_MAX_HEALTH.set(player, health);
	}
}

@Data
class WorldHealth {
	private final String name;
	private final double health;
}