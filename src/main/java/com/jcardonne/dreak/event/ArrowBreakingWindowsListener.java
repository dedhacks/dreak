package com.jcardonne.dreak.event;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.BlockIterator;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.remain.CompSound;

public class ArrowBreakingWindowsListener implements Listener {

	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		final Projectile projectile = event.getEntity();

		if (!(projectile instanceof Arrow || projectile.getShooter() instanceof Player))
			return;

		final World world = projectile.getWorld();
		final Arrow arrow = (Arrow) projectile;
		final BlockIterator blockIterator = new BlockIterator(world, arrow.getLocation().toVector(), arrow.getVelocity().normalize(), 0, 4);
		Block hitBlock = null;

		while(blockIterator.hasNext())
		{
			hitBlock = blockIterator.next();
			if(!hitBlock.getType().equals(Material.AIR)) {break;}
		}

		if (hitBlock != null && hitBlock.getType().toString().contains("GLASS")) {
			hitBlock.setType(Material.AIR);
			CompSound.GLASS.play(hitBlock.getLocation());
			projectile.remove();
		}
	}
}
