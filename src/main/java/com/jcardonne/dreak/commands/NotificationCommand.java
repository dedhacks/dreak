package com.jcardonne.dreak.commands;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.SimpleScoreboard;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;

public class NotificationCommand extends SimpleCommand {
	public NotificationCommand() {
		super("notif");

		setMinArguments(2);
		setUsage("<tab|score|title|action|toast> <input|...>");
		// /notif title Hello World|Check out our website
	}

	@Override
	protected void onCommand() {

		checkConsole();

		final String param = args[0].toLowerCase();
		final String[] inputs = joinArgs(1).split("\\|");
		final String primaryPart = inputs[0];
		final String secondaryPart = inputs.length == 1 ? "" : inputs[1];
		final Player player = getPlayer();

		if ("tab".equals(param) || "title".equals(param)) {
			checkBoolean(inputs.length == 2, "Usage /{label} {0} <primary>|<secondary>");

			if ("tab".equals(param))
				Remain.sendTablist(player, primaryPart, secondaryPart);
			else
				Remain.sendTitle(player, primaryPart, secondaryPart);
		} else if ("action".equals(param))
			Remain.sendActionBar(player, primaryPart);
		else if ("toast".equals(param))
			Remain.sendToast(player, primaryPart, CompMaterial.CAKE);
		else if ("score".equals(param)) {
			SimpleScoreboard scoreboard = new MyScoreboard();

			scoreboard.setTitle(primaryPart);
			scoreboard.addRows(secondaryPart);
			scoreboard.setUpdateDelayTicks(3 * 20);
			if (scoreboard.isViewing(player))
				scoreboard.hide(player);

			scoreboard.show(player);
		}
	}
}

class MyScoreboard extends SimpleScoreboard {

	@Override
	protected String replaceVariables(final Player player, final String message) {
		return message.replace("{online}", Remain.getOnlinePlayers().size() + "");
	}
}
