package com.jcardonne.dreak.commands;

import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.command.SimpleCommand;

public class PingCommand extends SimpleCommand {
	public PingCommand() {
		super("ping");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		Player player = getPlayer();

		//way 1: direct import
		//CraftPlayer craftPlayer = (CraftPlayer) player;
		//tell("Your ping: " + craftPlayer.getHandle().ping + "ms.");


		/*way 2 Classic reflection
		Class<?> craftPlayerClass = ReflectionUtil.getOBCClass(".entity.CraftPlayer");
		Method getHandleMethod = ReflectionUtil.getMethod(craftPlayerClass, "getHandle");
		Object entityPlayer = ReflectionUtil.invoke(getHandleMethod, getPlayer());
		int ping = ReflectionUtil.getFieldContent(entityPlayer, "ping");
		tell("Your ping (reflection): " + ping);*/

		/*Way3 the enhanced way
		Object entityPlayer = Remain.getHandleEntity(getPlayer());
		int ping = ReflectionUtil.getFieldContent(entityPlayer, "ping");
		tell("Your ping (reflection V2): " + ping);*/

		//Way4: the next level way
		tell("Your ping (next level way): " + PlayerUtil.getPing(getPlayer())); //do not use player variable <- create lag
	}
}
