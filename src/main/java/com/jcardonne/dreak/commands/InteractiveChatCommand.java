package com.jcardonne.dreak.commands;

import org.mineacademy.fo.ChatUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.SimpleComponent;

public class InteractiveChatCommand extends SimpleCommand {
	public InteractiveChatCommand() {
		super("interactivechat|ic");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		// ---------------------------------------
		//          Moderation tools
		// ---------------------------------------
		// [Day] Click to set this world time to day
		// [Night] Click to set this world time to night
		// [Hand] Click to show your hand item
		// [PM] Click to suggest a PM.
		SimpleComponent.of("&8" + Common.chatLineSmooth()).append(ChatUtil.center("Moderation Tools")).append("&8" + Common.chatLineSmooth())
				.append("\n&e[&6DAY&e]").onHover("Set time to Day").onClickRunCmd("/time set day")
				.append("\n&b[&3Night&b]").onHover("Set time to Night").onClickRunCmd("/time set night")
				.append("\n[Hand]").onHover(getPlayer().getItemInHand())
				.append("\n[PM]").onHover("Click to suggest a PM").onClickSuggestCmd("/tell " + sender.getName() + " Hello World!")
				.append("\n[WebSite]").onHover("Click to go on our new website").onClickOpenUrl("https://jcardonne.com")
				.send(sender);
	}
}
